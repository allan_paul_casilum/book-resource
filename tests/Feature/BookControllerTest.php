<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
class BookControllerTest extends TestCase
{
	use WithoutMiddleware;
    
	public function testIndex()
    {
        $response = $this->json('GET', '/api/v1/books');
        $response->assertStatus(200);
    }
}
