<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert(
			array(
                array(
					'name' => 'allan',
					'email' => 'allan@gmail.com',
					'password' => Hash::make('allan')
				),
				array(
					'name' => 'potpot',
					'email' => 'potpot@gmail.com',
					'password' => Hash::make('potpot')
				)
			)
		);
        
    }
}
