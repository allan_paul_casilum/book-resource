<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/login','API\V1\BooksController@accessToken')->name('login');


Route::group(['middleware' => ['web','auth:api']], function(){
	Route::prefix('v1')->group(function () {
		Route::post('books','API\V1\BooksController@store');
		Route::get('books', 'API\V1\BooksController@index');
		Route::put('books/{id}','API\V1\BooksController@update');
		Route::put('books/{id}','API\V1\BooksController@update');
		Route::delete('books/{id}', 'API\V1\BooksController@destroy');
		Route::post('books/search', 'API\V1\BooksController@search');
		Route::post('books/purchase', 'API\V1\PurchaseBookController@store');
	});
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});