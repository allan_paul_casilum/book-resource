<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseBook extends Model
{
	
	protected $table = 'purchase_book';
	    
	protected $fillable = ['books_id', 'users_id'];

}
