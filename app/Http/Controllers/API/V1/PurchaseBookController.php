<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\PurchaseBook;
use App\Books;
use App\User;
use App\Mail\NotifyPurchase;
class PurchaseBookController extends Controller
{
	
	/**
     * Get a validator for an incoming Book request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validations($request,$type){
        $errors = [];
        $error = false;
        if($type == "purchase books"){
            $validator = Validator::make($request->all(),[
                'books_id' => 'required',
                'users_id' => 'required'
            ]);
            if($validator->fails()){
                $error = true;
                $errors = $validator->errors();
            }
		}
        return ["error" => $error,"errors"=>$errors];
    }
	
	/**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function prepareResult($status, $data, $errors,$msg)
    {
        return ['status' => $status,'data'=> $data,'message' => $msg,'errors' => $errors];
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = $this->validations($request,"purchase books");
        if ($error['error']) {
            return $this->prepareResult(false, [], $error['errors'],"Error in purchasing books");
        } else {
			$books = Books::find($request->input('books_id'));
			if( $books ){
				
				//$purchased = new PurchaseBook;
				$purchased = PurchaseBook::Create($request->all());
				//$purchased->Create($request->all());
				
				$user_purchased = User::find($request->input('users_id'));
				$notify = array(
					'user_name' => $user_purchased->name,
					'book_title' => $books->title
				);
				
				$email_to = $request->user()->email;
				
				Mail::to($email_to)->send(new NotifyPurchase($notify));
				//dd($notify);
				return $this->prepareResult(true, $purchased, $error['errors'],"Books purchased");
			}else{
				return $this->prepareResult(false, [], $error['errors'],"Error in purchasing books, book not found");
			}
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
