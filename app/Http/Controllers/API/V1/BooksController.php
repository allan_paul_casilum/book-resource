<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Books;

class BooksController extends Controller
{
	
	public function accessToken(Request $request){
        $validate = $this->validations($request,"login");
        if($validate["error"]){
            return $this->prepareResult(false, [], $validate['errors'],"Error while validating user"); 
        }
        $user = User::where("email",$request->email)->first();
        if($user){
            if (Hash::check($request->password,$user->password)) {
                return $this->prepareResult(true, ["accessToken" => $user->createToken('Book App')->accessToken], [],"User Verified");
            }else{
                return $this->prepareResult(false, [], ["password" => "Wrong passowrd"],"Password not matched");  
            }
        }else{
            return $this->prepareResult(false, [], ["email" => "Unable to find user"],"User not found");
        }
        
    }
	
	 /**
     * Get a validator for an incoming Book request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validations($request,$type){
        $errors = [];
        $error = false;
        if($type == "login"){
            $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:255',
            'password' => 'required',
            ]);
            if($validator->fails()){
                $error = true;
                $errors = $validator->errors();
            }
        }elseif($type == "create books"){
            $validator = Validator::make($request->all(),[
                'users_id' => 'required',
                'title' => 'required',
                'price' => 'required'
            ]);
            if($validator->fails()){
                $error = true;
                $errors = $validator->errors();
            }
		}elseif($type == "update books"){
            $validator = Validator::make($request->all(),[
                'title' => 'filled',
                'price' => 'filled'
            ]);
            if($validator->fails()){
                $error = true;
                $errors = $validator->errors();
            }
        }elseif($type == "search books"){
            $validator = Validator::make($request->all(),[
                'q' => 'required'
            ]);
            if($validator->fails()){
                $error = true;
                $errors = $validator->errors();
            }
        }
        return ["error" => $error,"errors"=>$errors];
    }
	
	/**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function prepareResult($status, $data, $errors,$msg)
    {
        return ['status' => $status,'data'=> $data,'message' => $msg,'errors' => $errors];
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		return $this->prepareResult(true, $request->user()->books()->paginate(10), [],"All user books");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		//auto add user id
		$request->request->add(['users_id' => $request->user()->id]);
       	
		$error = $this->validations($request,"create books");
        if ($error['error']) {
            return $this->prepareResult(false, [], $error['errors'],"Error in creating books");
        } else {
            $book = $request->user()->books()->Create($request->all());
            return $this->prepareResult(true, $book, $error['errors'],"Books created");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
	/**
     * search the specified resource.
     *
     * @param  string  $q
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $error = $this->validations($request,"search books");
        if ($error['error']) {
            return $this->prepareResult(false, [], $error['errors'],"Error in searching books");
        } else {
            $book = Books::where('title', 'LIKE', '%' . $request->input('q') . '%')->get();
            if( count($book) > 0 ){
				return $this->prepareResult(true, $book, $error['errors'],"Search Books found");
			}else{
				return $this->prepareResult(true, $book, $error['errors'],"Search Books not found");
			}
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (!$id) {
			return $this->prepareResult(false, [], "unauthorized","You are not authenticated to edit this book");
        }
		$books = Books::find($id);
		
        if($books && $books->users_id == $request->user()->id){
         
		   $error = $this->validations($request,"update books");
            if ($error['error']) {
                return $this->prepareResult(false, [], $error['errors'],"error in updating data");
            } else {
                $book = $books->fill($request->all())->save();
                return $this->prepareResult(true, $book, $error['errors'],"updating data");
            }
        }else{
            return $this->prepareResult(false, [], "unauthorized","You are not authenticated to edit this book");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		if (!$id) {
			return $this->prepareResult(false, [], "unauthorized","provide ID");
        }
		$books = Books::find($id);
        if($books && $books->users_id == $request->user()->id){
            if ($books->delete()) {
                return $this->prepareResult(true, [], [],"Book deleted");
            }
        }else{
            return $this->prepareResult(false, [], "unauthorized","You are not authenticated to delete this book");
        }      
    }
}
